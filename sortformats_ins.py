fordered = ("raw", "bmp", "tiff", "ppm", "eps", "tga","pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1, format2):
   for x in range (0,len(fordered)):
       if format1 == format2:
           return False
       elif format1 == fordered[x]:
           format1 = x
       elif format2 == fordered[x]:
           format2 = x
   if format1 < format2:
        return True
   else:
        return  False

def find_lower_pos(formats, pivot):
    lower = pivot
    lista = []
    for pos in range(pivot, len(formats)):
        if lower_than(formats[pivot],formats[pos]) == False:
            formats[pivot],formats[pos] = formats[pos],formats[pivot]
            lower = [formats[pos]]
            lista = lista + lower
    return lista

def sort_formats(formats):
    for pivot in range (0,len(formats)):
        lista = find_lower_pos(formats,pivot)
    return lista

def main():
    import sys
    if len(sys.argv) < 2:
        print("Usage: python3 sortformats.py format1 format2 ...")
        return

    formats = sys.argv[1:]
    sort_formats(formats)
    print(' '.join(formats))

if __name__ == '__main__':
    main()
